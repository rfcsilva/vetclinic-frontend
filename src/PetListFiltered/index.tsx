import {Col, Row} from "react-bootstrap";
import React, {ChangeEvent, useEffect, useState} from "react";
import {GlobalState} from "../App/reducers";
import {fetchPets} from "./actions";
import {connect} from "react-redux";
import {BasicUser} from "../UserInfo/UserInfo";
import {Link} from "react-router-dom";
import {Pet} from "./reducers";

export const PetList = (props:{pets:Pet[]} ) =>
    <ul>
        { props.pets.map((pet:Pet) =>  <li value={pet.id} key={pet.id}><Link to={`/pets/${pet.id}`}>{pet.name}</Link></li> )}
    </ul>;

const ProtoFilteredPetList = (props:{pets: Pet[], loadPets:(filter:string,  user: BasicUser, userType:string)=>void, user: BasicUser}) => {

    const [ filter, setFilter] = useState("");
    useEffect(() => props.loadPets(filter, props.user, props.user.type), [filter]);

    let handle = (e:ChangeEvent<HTMLInputElement>) => setFilter(e.target.value);

    return (
        <Row>
            <Col className={'pl-5'}>

                {props.user.type !== 'CLIENT' && <>

                    <Row>
                        <label>Search by name: </label>
                        <input className={'ml-2'} onChange={handle} value={filter}/>
                    </Row>

                </>
                }

                <Row>
                    <PetList pets={props.pets} />
                </Row>
            </Col>
        </Row>
    );

};

const mapStateToProps = (state:GlobalState) => ({pets:state.pets.pets, user: state.signIn.user});
const mapDispatchToProps = (dispatch:any) => ({
    loadPets:(filter:string, user: BasicUser, userType:string) => { dispatch(fetchPets(user, filter, userType))}
});
export const FilteredPetList = connect(mapStateToProps,mapDispatchToProps)(ProtoFilteredPetList);