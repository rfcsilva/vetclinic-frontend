import {Action} from "redux";
import {getData} from "../RestClient";
import {BasicPet, buildPet, Pet} from "./reducers";
import {BasicUser} from "../UserInfo/UserInfo";
import {DELETE_PET} from "../PetDetails/actions";

export const ADD_PET = 'ADD_PET';
export const REQUEST_PETS = 'REQUEST_PETS';
export const RECEIVE_PETS = 'RECEIVE_PETS';
export const FILTER_PETS = 'FILTER_PETS';

export interface AddPetAction extends Action { data: Pet }
export interface ReceivePetAction extends Action { data: Pet[]}

export const addPet = (pet:BasicPet) => ({type:ADD_PET, data:pet});
export const removePet = (pet:BasicPet) => ({type:DELETE_PET, data:pet});
export const requestPets = () => ({type: REQUEST_PETS});
export const receivePets = (data:Pet[]) => ({type: RECEIVE_PETS, data:data});

interface PetWithOwner { pet:BasicPet, owner:BasicUser }

function receiveAllPets(data: PetWithOwner[]) : Pet[]{

    let petsToReturn : Pet[] = [];
    for (let i = 0; i<data.length; i++)
        petsToReturn.push({id:data[i].pet.id, name:data[i].pet.name, species:data[i].pet.species, age:data[i].pet.age,
            description:data[i].pet.description, medical_notes:data[i].pet.medical_notes, active:data[i].pet.active, owner:data[i].owner});

    return petsToReturn;
}

export function fetchPets(user: BasicUser, filter:string, userType:string) {
    return (dispatch:any) => {
        dispatch(requestPets());
        let url: string = "";
        if (userType!=="CLIENT") {
            url= `/api/pets?search=${filter}`;
            return getData<PetWithOwner[]>(url, [])
                .then(data => { console.log(data); data && dispatch(receivePets(receiveAllPets(data))) })
        }
        else {
                url= `/api/clients/${user.id}/pets`;
            return getData<BasicPet[]>(url, [])
                .then(data => { console.log(data); data && dispatch(receivePets( data.map( p => buildPet(p, user))))})
            }

        // notice that there is an extra "pet" in the path above which is produced
        // in this particular implementation of the service. {pet: Pet, appointments:List<AppointmentDTO>}
    }
}
