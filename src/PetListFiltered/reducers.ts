import {Action} from "redux";
import {ReceivePetAction, RECEIVE_PETS, ADD_PET, REQUEST_PETS, FILTER_PETS, AddPetAction} from './actions';
import {BasicUser} from "../UserInfo/UserInfo";


export interface AllPetsState { pets: Pet[], isFetching: boolean }
export interface NewPet { id:number, name:string, species: string, age: number, description: string, medical_notes: string,active: boolean }


export interface Pet { id:number, name:string, species: string, age: number, description: string, medical_notes: string,active: boolean, owner: BasicUser}
export interface BasicPet { id:number, name:string, species: string, age: number, description: string, medical_notes: string,active: boolean}
export interface PetState { pets: Pet[], isFetching: boolean }


export function buildBasicPet(id: number, name: string, species: string, age: number, description: string): BasicPet {
    return {id: id, name: name, species:species, age:age, description: description, medical_notes: "swag", active: true};
}

export function buildPet( basicPet: BasicPet, owner: BasicUser ): Pet {
    return { id: basicPet.id, name: basicPet.name, species: basicPet.species, age: basicPet.age, description: basicPet.description, medical_notes: basicPet.medical_notes, active: basicPet.active, owner: owner };
}

export function buildNewPet(name: string, species: string, age: number, description: string): NewPet {
    return {id: 0, name: name, species:species, age:age, description: description, medical_notes: "swag", active: true};
}

function loadPets(): Pet[] {
    let petsJson = localStorage.getItem('pets');
    if(petsJson === null)
        return [] as Pet[];
    else
        return JSON.parse(petsJson);
}

const initialState = {
    pets: loadPets(),
    isFetching:false,
};

function filteredPetListReducer(state:PetState = initialState, action:Action):PetState {
    switch (action.type) {

        case ADD_PET:
            return {...state, pets: state.pets.concat( (action as AddPetAction).data) };
        case FILTER_PETS:
            return {...state };
        case REQUEST_PETS:
            return {...state, isFetching:true};
        case RECEIVE_PETS:
            let newPets : Pet[] = (action as ReceivePetAction).data;
            newPets.map( p => console.log(p));
            localStorage.setItem('pets', JSON.stringify(newPets));

            return {isFetching:false, pets: (action as ReceivePetAction).data };
        default:
            return state
    }
}

export default filteredPetListReducer