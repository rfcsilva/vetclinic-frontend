import React, {useState} from "react";
import {Button, Col, Form, FormControl, FormControlProps, Row} from "react-bootstrap";
import {Pet} from "../PetListFiltered/reducers";

const AppointmentOngoing = (props: { pet: Pet }) => {
    const [ description, setDescription ] = useState("");
    let descriptionChangeHandler = (e: React.FormEvent<FormControl & FormControlProps>) => { setDescription( e.currentTarget.value as string) };

    let submitHandler = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        //let updateAppointment: updateAppointment = updateAppointment( ,description);
        //props.performPetCreation(newPet, props.currentUser.id);

        setDescription("");

    };

    return (
        <>
            <Row className={'pl-5'}>
                <Col>
                    <Row className={'mb-2'}>
                        <h4>{props.pet.name}</h4>
                    </Row>
                    <Row>
                        <h6>Age: {props.pet.age} months old</h6>
                    </Row>
                    <Row>
                        <h6>Description: {props.pet.description}</h6>
                    </Row>
                    <Row>
                        <h6>Medical notes: {props.pet.medical_notes}</h6>
                    </Row>
                </Col>
            </Row>
            <Form className={'pl-3'} onSubmit={submitHandler}>

                <Form.Group as={Col} controlId="formGridState">
                    <Form.Label>Description of the appointment</Form.Label>
                    <Form.Control onChange={descriptionChangeHandler} type="description" placeholder="Enter the description of what was done in the appointment"/>
                </Form.Group>

                <Button className={'ml-3'} variant="primary" type="submit">
                    Submit
                </Button>

            </Form>
        </>);

};

export default AppointmentOngoing