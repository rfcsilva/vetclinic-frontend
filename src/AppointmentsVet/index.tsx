import React, {ChangeEvent, useEffect, useState} from "react";
import {connect} from "react-redux";
import {GlobalState} from "../App/reducers";
import Appointment from "../AppointmentClient/Appointment";
import {Button, Col, Container, Row} from "react-bootstrap";
import {RichAppointment} from "../AppointmentsClient/reducers";
import {fetchAppointments} from "../AppointmentsClient/actions";
import AppointmentVet from "../AppointmentVet";


export const AppointmentVetList = (props: { appointments: RichAppointment[] }) =>

    <Container fluid={true} className={'p-2'}>
        <ul>
            {props.appointments.map((appointment: RichAppointment) => <li key={appointment.appointment.id}><AppointmentVet
                appointment={appointment}/></li>)}
        </ul>
    </Container>;

const ProtoFilteredVetAppointmentList = (props: { appointments: RichAppointment[], loadAppointments: (filter: string) => void }) => {
    const [filter, setFilter] = useState("");
    let handle = (e: ChangeEvent<HTMLInputElement>) => setFilter(e.target.value);

    useEffect(() => props.loadAppointments(filter), [filter]);

    let myStyle={
        backgroundColor:'red'
    };

    return <>
        <Row className={'pl-5'}>
            <Col>
                <Row>
                    <h1>My Appointments</h1>
                </Row>

                <Row>
                    <AppointmentVetList appointments={props.appointments}/>
                </Row>
            </Col>
        </Row>
    </>;
};

const mapStateToProps = (state: GlobalState) => ({appointments: state.appointments.appointments});
const mapDispatchToProps = (dispatch: any) => ({
    loadAppointments: (filter: string) => {
        dispatch(fetchAppointments(3))
    }
});

export const FilteredVetAppointmentList = connect(mapStateToProps, mapDispatchToProps)(ProtoFilteredVetAppointmentList);
