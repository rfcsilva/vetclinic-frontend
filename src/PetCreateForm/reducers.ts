import {Action} from "redux";
import {CREATE_PET, RECEIVE_PET_CREATION_REPLY_ERROR, RECEIVE_PET_CREATION_REPLY_OK} from "./actions";

const initialState = {
    success: true,
    isPosting: false
};

function petFormReducer(state= initialState, action:Action) {
    switch (action.type) {
        case CREATE_PET:
            return {...state, isPosting:true};
        case RECEIVE_PET_CREATION_REPLY_OK:
            return {...state, isFetching:false, success: true};
        case RECEIVE_PET_CREATION_REPLY_ERROR:
            return {...state, isFetching:false, success: false};
        default:
            return state
    }
}

export default petFormReducer