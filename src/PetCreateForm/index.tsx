import React, {FormEvent, useState} from "react";
import {Button, Col, Container, Form, FormControl, FormControlProps, Row} from "react-bootstrap";
import {connect} from "react-redux";
import {GlobalState} from "../App/reducers";
import {BasicUser} from "../UserInfo/UserInfo";
import {requestPetCreation} from "./actions";
import {buildNewPet, NewPet} from "../PetListFiltered/reducers";


const ProtoPetCreateForm = (props: { performPetCreation: (pet: NewPet, owner: number) => void, currentUser: BasicUser }) => {

    const [name, setName] = useState("");
    const [species, setSpecies] = useState("");
    const [age, setAge] = useState("");
    const [description, setDescription] = useState("");

    let nameChangeHandler = (e: React.FormEvent<FormControl & FormControlProps>) => {
        setName(e.currentTarget.value as string)
    };
    let speciesChangeHandler = (e: React.FormEvent<FormControl & FormControlProps>) => {
        setSpecies(e.currentTarget.value as string)
    };
    let ageChangeHandler = (e: React.FormEvent<FormControl & FormControlProps>) => {
        setAge(e.currentTarget.value as string)
    };
    let descriptionChangeHandler = (e: React.FormEvent<FormControl & FormControlProps>) => {
        setDescription(e.currentTarget.value as string)
    };

    let submitHandler = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        let newPet: NewPet = buildNewPet(name, species, parseInt(age), description);
        props.performPetCreation(newPet, props.currentUser.id);

        setName("");
        setSpecies("");
        setAge("");
        setDescription("");
    };

    return (
        <>
            <Container>
                <Row>
                    <Col>
                        <Form onSubmit={submitHandler}>
                            <Form.Row>
                                <Form.Group as={Col} controlId="formGridName">
                                    <Form.Label>Pet name</Form.Label>
                                    <Form.Control onChange={nameChangeHandler} type="name" placeholder="Enter name"/>
                                </Form.Group>

                                <Form.Group as={Col} controlId="formGridSpecies">
                                    <Form.Label>Species</Form.Label>
                                    <Form.Control onChange={speciesChangeHandler} type="name" placeholder="Species"/>
                                </Form.Group>
                            </Form.Row>

                            <Form.Group controlId="formGridAddress1">
                                <Form.Label>Age of your pet (in months)</Form.Label>
                                <Form.Control onChange={ageChangeHandler} type="number" min="1" placeholder="Age"/>
                            </Form.Group>

                            <Form.Group controlId="formGridDescription">
                                <Form.Label>Brief physical description of your pet</Form.Label>
                                <Form.Control onChange={descriptionChangeHandler} as="textarea" rows="3"
                                              placeholder="Physical description"/>
                            </Form.Group>

                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                        </Form>
                    </Col>
                </Row>
            </Container>
        </>);

};

//TypeScript #Claps Claps lol
// @ts-ignore
const mapStateToProps = (state: GlobalState) => ( { currentUser: state.signIn.user});
const mapDispatchToProps = (dispatch: any) => ({
    performPetCreation: (pet: NewPet, owner: number) => {
        dispatch(requestPetCreation(pet, owner))
    }
});

const PetCreateForm = connect(mapStateToProps, mapDispatchToProps)(ProtoPetCreateForm);
export default PetCreateForm