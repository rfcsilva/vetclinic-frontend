import {Action} from "redux";
import {postData} from "../RestClient";
import {NewPet, Pet} from "../PetListFiltered/reducers";
import {addPet} from "../PetListFiltered/actions";

export const CREATE_PET = 'CREATE_PET';
export const RECEIVE_PET_CREATION_REPLY_OK = 'RECEIVE_PET_CREATION_REPLY';
export const RECEIVE_PET_CREATION_REPLY_ERROR = 'RECEIVE_PET_CREATION_REPLY_ERROR';

export interface CreatePetAction extends Action { }
export interface ReceivePetReplyOk extends Action { data: Pet }
export interface ReceivedPetReplyError extends Action { }

export const createPet = () => ({type:CREATE_PET});
export const receivePetReplyOk = (data:Pet) => ({type: RECEIVE_PET_CREATION_REPLY_OK, data:data});
export const receivedPetReplyError = () => ({type: RECEIVE_PET_CREATION_REPLY_ERROR});

export function requestPetCreation(pet: NewPet, user_id: number) {
    return (dispatch: any) => {
        dispatch(createPet());
        return postData(`/api/clients/${user_id}/pets`, pet)
            .then(data => {
                data && dispatch(receivePetReplyOk(data) && dispatch(addPet(data)))
            })
            .catch(() => dispatch(receivedPetReplyError()))
    }
}