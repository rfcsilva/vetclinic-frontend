import React from "react";
import {Col, Row} from "react-bootstrap";
import UserInfo, {User} from "../UserInfo/UserInfo";


const GridStaff = (props:{staff:User[]}) => {


    return (<Row>
            {props.staff.map( u => <Col key={u.id}> <UserInfo user={u}/> </Col>)}
            </Row>
    );
};

export default GridStaff;