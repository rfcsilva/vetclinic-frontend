import React from "react";
import {connect} from "react-redux";
import {GlobalState} from "../App/reducers";
import {Button, Col, Row} from "react-bootstrap";
import {FilteredPetList} from "../PetListFiltered";
import {Link} from "react-router-dom";
import {BasicUser} from "../UserInfo/UserInfo";


const ProtoPetView = (props: { user: BasicUser }) => {

    return <>
        <Row className={'pl-5'}>
            <Col sm={4}>
                <Row>
                    <Link to={'/add_pet'}>
                        <Button variant="info">
                            Add Pet
                        </Button>
                    </Link>
                </Row>

                {props.user.type === 'CLIENT' && <>
                    <Row>
                        <h1>My Pets</h1>
                    </Row>
                </>}

                <FilteredPetList/>
            </Col>
        </Row>
    </>;
};

const mapStateToProps = (state: GlobalState) => ({user: state.signIn.user});
export const PetsView = connect(mapStateToProps)(ProtoPetView);