const NUMB_TRIES : number = 2;

export function getData<T>(url:string, defaultValue:T):Promise<T> {
    let auth = {};
    let token = localStorage.getItem('jwt');
    if (token) auth = {'Authorization':token};

    // sign out in case of unauthorized access (expired session)
    return fetch(url, {
        method: "GET",
        mode: "cors",
        cache: "no-cache",
        headers: {
            ...auth,
            'Content-Type': 'application/json'
        }
    })
        .then(response => {
            if (response.ok)
                return response.json();
            else {
                console.log(`Error: ${response.status}: ${response.statusText}`);
                console.log(response.body);
                return new Promise<T>((resolve, reject) => resolve(defaultValue))
            }
        })
        .catch(reason => {
            console.log(reason);
        });
}

export function postData(url: string, data: any) {
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    return fetch(url,
        {method:'POST',
            headers: myHeaders,
            body: JSON.stringify(data)})
        .then( response => {
            if( response.ok )
                return response.json();
            else {
                console.log(`Error: ${response.status}: ${response.statusText}`);
                return null;
                // and add a message to the Ui: wrong password ?? other errors?
            }
        })
        .catch( err => { console.log(err); return null })
}

export function putData(url: string, data: any) {
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    console.log('Mekei');
    console.log(data);

    return fetch(url,
        {method:'PUT',
            headers: myHeaders,
            body: JSON.stringify(data)})
        .then( response => {
            if( response.ok )
                return response.json();
            else {
                console.log(`Error: ${response.status}: ${response.statusText}`);
                return null;
                // and add a message to the Ui: wrong password ?? other errors?
            }
        })
        .catch( err => { console.log(err); return null })
}

export function deleteData(url: string) {
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    console.log('DELETING');

    return fetch(url,
        {method:'DELETE',
            headers: myHeaders})
        .then( response => {
            if( response.ok) {
                console.log("muita naisse")
                return response.json();
            }
            else {
                console.log(`Error: ${response.status}: ${response.statusText}`);
                return null;
                // and add a message to the Ui: wrong password ?? other errors?
            }
        })
        .catch( err => { console.log(err); return null })
}



