import React, {ChangeEvent, useEffect, useState} from "react";
import {RichAppointment} from "../AppointmentsClient/reducers";
import Appointment from "../AppointmentClient/Appointment";


const DropdownSlot = (props: { appointments: RichAppointment[]}) => {


    let myStyle={
        backgroundColor: 'blue'
    };

    return (
        <>
            {props.appointments.map((appointment: RichAppointment) => <option key={appointment.appointment.id}> {new Date(appointment.appointment.start_time).toLocaleDateString()} at {new Date(appointment.appointment.start_time).toLocaleTimeString()}
            </option>)}
        </>);

};

export default DropdownSlot