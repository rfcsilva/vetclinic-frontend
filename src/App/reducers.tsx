import {combineReducers} from "redux";
import appointmentReducer, {AppointmentState} from "../AppointmentsClient/reducers";
import signInReducer, {SignInState} from "../LoginForm/reducers";
import employeeReducer, {EmployeesState} from "../VisitorView/reducers";
import filteredVetListReducer, {VetState} from "../VetListFiltered/reducers";
import filteredClientListReducer, {ClientState} from "../ClientListFiltered/reducers";
import filteredPetListReducer, {PetState} from "../PetListFiltered/reducers";
import petEditFormReducer, {PetEditingState} from "../PetEditForm/reducers";

export interface GlobalState { pets: PetState, appointments: AppointmentState, signIn: SignInState, employees: EmployeesState, vets: VetState, clients: ClientState, petEdition: PetEditingState }

const reducer = combineReducers({pets: filteredPetListReducer, appointments: appointmentReducer,signIn:signInReducer, employees: employeeReducer, vets:filteredVetListReducer,
                                clients:filteredClientListReducer, petEdition: petEditFormReducer});

export default reducer
