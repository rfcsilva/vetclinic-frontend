import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.css';
import reducer, {GlobalState} from "./reducers";
import {applyMiddleware, createStore} from "redux";
import thunk from 'redux-thunk';
import {connect, Provider} from "react-redux";
import {createLogger} from "redux-logger";
import {AppHeader} from "../AppHeader/AppHeader";
import {FilteredAppointmentList} from "../AppointmentsClient";
import {VisitorView} from "../VisitorView";
import {FilteredVetAppointmentList} from "../AppointmentsVet";
import {BasicUser} from "../UserInfo/UserInfo";
import PetCreateForm from "../PetCreateForm";
import {PetsView} from "../PetsView"
import {PetDetails} from "../PetDetails";
import {FilteredPetList} from "../PetListFiltered";
import PetEditForm from "../PetEditForm";
import {FilteredClientList} from "../ClientListFiltered";
import {ClientDetails} from "../ClientDetails";
import {AppointmentDetails} from "../AppointmentClientDetails";


const ProtoPage = (props: { isSignedIn: boolean, user: BasicUser }) => {
    console.log('Is Logged In? ' + props.isSignedIn);

    if (props.isSignedIn) {

        console.log(props.user);
        console.log(props.user.type + ' ' + props.user.username);

        switch (props.user.type) {
            case 'VETERINARIAN' :
                console.log('Rendering Vet View');
                return (<FilteredVetAppointmentList/>);
            case 'ADMINISTRATOR':
                console.log('Rendering Admin View');
                return (<FilteredPetList/>);
            case 'CLIENT' :
                console.log('Rendering Client View');
                return (<PetsView/>);
            default:
                return (<VisitorView/>);
        }
    } else
        return (<VisitorView/>);


};

const mapStateToProps = (state: GlobalState) => ({isSignedIn: state.signIn.isSignedIn, user: state.signIn.user});
const Page = connect(mapStateToProps)(ProtoPage);

const logger = createLogger();
let store = createStore(reducer, applyMiddleware(thunk, logger));

const App = () => {
    return (
        <Provider store={store}>
            <Router>
                <AppHeader/>
                <Route path={"/add_pet"} exact={true} component={PetCreateForm}/>
                <Route path={"/apps"} exact={true} component={FilteredAppointmentList}/>
                <Route path={"/apps/:id"} exact={true} component={AppointmentDetails}/>
                <Route path={"/pets"} exact={true} component={PetsView}/>
                <Route path={"/allpets"} exact={true} component={FilteredPetList}/>
                <Route path={"/pets/:id"} exact={true} component={PetDetails}/>
                <Route path={"/pets/:id/edit"} exact={true} component={PetEditForm}/>
                <Route path={"/clients"} exact={true} component={FilteredClientList}/>
                <Route path={"/clients/:id"} exact={true} component={ClientDetails}/>
                <Route path={"/pets/:id/appointments"} exact={true} component={FilteredAppointmentList}/>
                <Route path="/" exact={true} component={Page}/>
            </Router>
        </Provider>
    );
};

export default App;
