import {RichAppointment} from "./reducers";
import {Action} from "redux";
import {getData} from "../RestClient";

export const ADD_APPOINTMENT = 'ADD_APPOINTMENT';
export const REQUEST_APPOINTMENTS = 'REQUEST_APPOINTMENTS';
export const RECEIVE_APPOINTMENTS = 'RECEIVE_APPOINTMENTS';

export interface AddAppointmentAction extends Action { name:string }
export interface ReceiveAppointmentAction extends Action { data:RichAppointment[] }

export const addAppointment = (appointment:RichAppointment) => ({type:ADD_APPOINTMENT, data:appointment});
export const requestAppointments = () => ({type: REQUEST_APPOINTMENTS});
export const receiveAppointments = (data:RichAppointment[]) => ({type: RECEIVE_APPOINTMENTS, data:data});


export function fetchAppointments(pet_id: number) {
    return (dispatch:any) => {
        dispatch(requestAppointments());
        return getData<RichAppointment[]>(`/api/pets/${pet_id}/appointments`, [])
            .then(data => { console.log(data); data && dispatch(receiveAppointments(data)) })
        // notice that there is an extra "pet" in the path above which is produced
        // in this particular implementation of the service. {pet: Pet, appointments:List<AppointmentDTO>}
    }
}
