import {Action} from "redux";
import {ReceiveAppointmentAction, RECEIVE_APPOINTMENTS, ADD_APPOINTMENT, REQUEST_APPOINTMENTS} from './actions';
import {BasicUser, buildBasicUser} from "../UserInfo/UserInfo";
import * as faker from "faker";
import {Pet} from "../PetListFiltered/reducers";

interface Appointment { id:number, start_time: number,description: string,status: String, duration: number }
export interface RichAppointment { vet: BasicUser, pet : Pet, client: BasicUser, appointment: Appointment }

export interface AppointmentState { appointments: RichAppointment[], isFetching: boolean }

export let apps = createAppointments();

export interface AppointmentState { appointments: RichAppointment[], isFetching: boolean }

function buildAppointment(): Appointment {
    return { id: faker.random.number(1000), start_time: faker.date.future(1).getTime() , description: 'Consulta de rotina', status: 'Agendada', duration: 30*60*1000 }
}

function buildRichAppointment() : RichAppointment {
    let name = faker.name.firstName();
    let name2 = faker.name.firstName();
    let client = buildBasicUser( name2 + 'xxx', name2, "CLIENT" );
    return { vet: buildBasicUser( name+'xxx', name, "EMPLOYEE" ), pet: {} as Pet, client: client, appointment: buildAppointment() }
}

function createAppointments() {

    let toReturn: RichAppointment[] = [];
    for(let i = 0; i < 5; i++){
        toReturn.push( buildRichAppointment() );
    }

    return toReturn;
}

function myFilter(filter:string): RichAppointment[] {

    let returnApps = [];

    for(let i = 0; i< apps.length; i++){
        if (apps[i].pet.name.toUpperCase().startsWith(filter.toUpperCase())){
            returnApps.push(apps[i])
        }
    }

    return returnApps;

}

const initialState = {
    appointments: [buildRichAppointment()],
    isFetching:false,
};


function appointmentReducer(state:AppointmentState = initialState, action:Action):AppointmentState {
    switch (action.type) {
        case ADD_APPOINTMENT:
            return {...state, appointments:[...state.appointments, buildRichAppointment()]};
        case REQUEST_APPOINTMENTS:
            return {...state, isFetching:true};
        case RECEIVE_APPOINTMENTS:
            return {...state, isFetching:false, appointments: (action as ReceiveAppointmentAction).data };
        default:
            return state
    }
}

export default appointmentReducer