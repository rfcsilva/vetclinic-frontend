import React, {ChangeEvent, useEffect, useState} from "react";
import {connect} from "react-redux";
import {fetchAppointments} from "./actions";
import {GlobalState} from "../App/reducers";
import {RichAppointment} from "./reducers";
import {Button, Col, Container, Row} from "react-bootstrap";
import Appointment from "../AppointmentClient/Appointment";
import {Pet} from "../PetListFiltered/reducers";
import {match} from "react-router";
import {findPet} from "../PetDetails";
import {BasicUser} from "../UserInfo/UserInfo";
import {Link} from "react-router-dom";


export const AppointmentList = (props: { appointments: RichAppointment[] }) =>

    <Container fluid={true} className={'p-2'}>
        <ul>
            {props.appointments.map((appointment: RichAppointment) => <Link to={`/apps/${appointment.appointment.id}`}><li key={appointment.appointment.id}><Appointment
                appointment={appointment}/></li></Link>)}
        </ul>
    </Container>;

const ProtoFilteredAppointmentList = (props: { appointments: RichAppointment[], user:BasicUser, id: number, loadAppointments: (pet_id: number) => void }) => {
    const [filter, setFilter] = useState("");
    let handle = (e: ChangeEvent<HTMLInputElement>) => setFilter(e.target.value);
    //setTimeout(()=>props.loadAppointments(props.id), 1000*60*3)
    //useEffect(() => props.loadAppointments(props.id), [filter]);

    let myStyle={
        backgroundColor:'red'
    };

    return <>
        <Row className={'p-5'}>
            <Col>
                <Row className={'p-3'}>
                    {props.user.type=== 'CLIENT' && <Button  variant="info">Create Appointment</Button>}
                </Row>
                <Row>
                    <h1>Appointments</h1>
                </Row>

                <Row>
                    <AppointmentList appointments={props.appointments}/>
                </Row>
            </Col>
        </Row>
    </>;
};

const mapStateToProps = (state: GlobalState, ownProps:any) => ({appointments: state.appointments.appointments, id:ownProps.match.params.id, user:state.signIn.user});
const mapDispatchToProps = (dispatch: any) => ({
    loadAppointments: (pet_id:number) => {
        dispatch(fetchAppointments(pet_id))
    }
});

export const FilteredAppointmentList = connect(mapStateToProps, mapDispatchToProps)(ProtoFilteredAppointmentList);
