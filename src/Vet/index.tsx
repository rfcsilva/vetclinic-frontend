import React, {useEffect} from "react";
import {connect} from "react-redux";
import {GlobalState} from "../App/reducers";
import {Col, Row} from "react-bootstrap";
import {PetList} from "../PetListFiltered";
import {BasicUser} from "../UserInfo/UserInfo";
import {fetchPets} from "../PetListFiltered/actions";
import {Pet} from "../PetListFiltered/reducers";


const ProtoVetView = (props: { pets: Pet[], user: BasicUser, loadPets: (filter: string, user_id: number) => void }) => {

    //useEffect(() => props.loadPets('', props.user.id), []);

    return <>
        <Row className={'pl-5'}>
            <Col sm={4}>
                <Row>
                    <h1>List of veterinarians</h1>
                </Row>
                <PetList pets={props.pets}/>
            </Col>
        </Row>
    </>;
};

const mapStateToProps = (state: GlobalState) => ({pets: state.pets.pets, user: state.signIn.user});
const mapDispatchToProps = (dispatch: any) => ({
    loadPets: (filter: string, user_id: number) => {
       // dispatch(fetchPets(user_id, filter))
    }
});
export const ClientVetsView = connect(mapStateToProps, mapDispatchToProps)(ProtoVetView);
