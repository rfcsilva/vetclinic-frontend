import {Action} from "redux";
import {getData} from "../RestClient";
import {GroupedEmployees} from "../UserInfo/UserInfo";

const EMPLOYEES_ENDPOINT = '/api/employees';

export const REQUEST_EMPLOYEES = 'REQUEST_EMPLOYEES';
export const RECEIVE_EMPLOYEES = 'RECEIVE_EMPLOYEES';

export interface ReceiveEmployeesAction extends Action { data: GroupedEmployees }

export const requestEmployees = () => ({type: REQUEST_EMPLOYEES});
export const receiveEmployees = (data: GroupedEmployees) => ({type: RECEIVE_EMPLOYEES, data:data});

export function fetchEmployees() {
    return (dispatch:any) => {
        dispatch(requestEmployees());
        return getData(EMPLOYEES_ENDPOINT, {admins:[], vets:[]} as GroupedEmployees)
            .then(data => { data && dispatch(receiveEmployees(data)) });
        // notice that there is an extra "pet" in the path above which is produced
        // in this particular implementation of the service. {pet: Pet, appointments:List<AppointmentDTO>}
    }
}