import React, {useEffect, useState} from "react";
import {Col, Row} from "react-bootstrap";
import LoginForm from "../LoginForm/LoginForm";
import GridStaff from "../GridStaff/GridStaff";
import './styles.css';
import {GroupedEmployees} from "../UserInfo/UserInfo";
import {GlobalState} from "../App/reducers";
import {connect} from "react-redux";
import {fetchEmployees} from "./actions";


const ProtoVisitorView = (props:{ employees: GroupedEmployees, loadEmployees:()=>void} ) => {

    useEffect(() => props.loadEmployees(), []);

    return (
        <Row className={'topElem'}>
            <Col className={'col-lg-3'}>
                <LoginForm/>
            </Col>
            <Col>
                <Row>
                    <Col>
                        <Row>
                            <h3>Our Veterinarian Team:</h3>
                        </Row>
                        <Row>
                            <GridStaff staff={props.employees.vets}/>
                        </Row>
                    </Col>
                </Row>
                <Row className={'bottomElem'}>
                    <Col>
                        <Row>
                            <h3>Our Management Team:</h3>
                        </Row>
                        <Row>
                            <GridStaff staff={props.employees.admins}/>
                        </Row>
                    </Col>
                </Row>
            </Col>
        </Row>
    );
};

const mapStateToProps = (state:GlobalState) => ({employees: state.employees.employees, fetched: state.employees.isFetching});
const mapDispatchToProps = (dispatch:any) => ({loadEmployees:() => { dispatch(fetchEmployees())}});
export const VisitorView = connect(mapStateToProps,mapDispatchToProps)(ProtoVisitorView);