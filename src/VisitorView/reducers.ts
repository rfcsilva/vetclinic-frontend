import {Action} from "redux";
import {RECEIVE_EMPLOYEES, ReceiveEmployeesAction, REQUEST_EMPLOYEES} from "./actions";
import {GroupedEmployees} from "../UserInfo/UserInfo";

export interface EmployeesState { employees: GroupedEmployees, isFetching: boolean }

const initialState = {
    employees:{ admins:[], vets:[] } as GroupedEmployees,
    isFetching:false,
};

function employeeReducer(state: EmployeesState = initialState, action:Action): EmployeesState {
    switch (action.type) {
        case REQUEST_EMPLOYEES:
            return {...state, isFetching:true};
        case RECEIVE_EMPLOYEES:
            return {...state, isFetching:false, employees: (action as ReceiveEmployeesAction).data};
        default:
            return state
    }
}

export default employeeReducer;