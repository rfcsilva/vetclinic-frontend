import React from "react";
import {Pet} from "../PetListFiltered/reducers";



const DropdownPet = (props: { pet: Pet[]}) => {


    let myStyle={
        backgroundColor: 'blue'
    };

    return (
        <>
            {props.pet.map((pet: Pet) => <option key={pet.id}> {pet.name}
            </option>)}
        </>);

};

export default DropdownPet