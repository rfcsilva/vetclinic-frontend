import React from "react";
import Nav from "react-bootstrap/Nav";
import {Link} from "react-router-dom";

import './TopNavBar.css'

const TopNavbar = (props: { isSignedIn: boolean, userType:string }) => {

    function LoggedInExtension() {
        return <>
            <Nav.Item>
                <Link to={"/apps"}> Appointments </Link>
            </Nav.Item>
            <Nav.Item>
                <Link to={"/pets"}> Pets </Link>
            </Nav.Item>
        </>
    }

    function LoggedInExtensionAdmin() {
        return <>
            <Nav.Item>
                <Link to={"/clients"}> Clients </Link>
            </Nav.Item>
            <Nav.Item>
                <Link to={"/allpets"}> Pets </Link>
            </Nav.Item>
        </>
    }

    return (
        <Nav>
            <Nav.Item>
                <Link to={"/"}> About us </Link>
            </Nav.Item>
            <Nav.Item>
               <Link to={"/"}> Animal Health </Link>
            </Nav.Item>

            {props.isSignedIn && props.userType==='CLIENT' && <LoggedInExtension/> /* If is signed in add some items to nav */}
            {props.isSignedIn && props.userType==='ADMINISTRATOR' && <LoggedInExtensionAdmin/> /* If is signed in add some items to nav */}

            <Nav.Item>
                <Link to={"/"}>Contacts</Link>
            </Nav.Item>
        </Nav>
    );
};

export default TopNavbar;

