import React from "react";
import {RichAppointment} from "../AppointmentsClient/reducers";
import {Button, Col, Form, Container} from "react-bootstrap";
import DropdownPet from "../DropdownPet";
import DropdownSlot from "../DropdownSlot";
import {Pet} from "../PetListFiltered/reducers";



const AppointmentCreateForm = (props: { appointments: RichAppointment[], pets:Pet[] }) => {

    return (
        <>
            <Container>
                <Form className={'pl-2'}>
                    <Form.Group as={Col} controlId="formGridState">
                        <Form.Label>Select the pet</Form.Label>
                        <Form.Control as="select">
                            <DropdownPet pet={props.pets}/>
                        </Form.Control>
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridState">
                        <Form.Label>Select the veterinarian</Form.Label>
                        <Form.Control as="select">
                            <option>Choose veterinarian</option>
                        </Form.Control>
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridState">
                        <Form.Label>Select the slot</Form.Label>
                        <Form.Control as="select">
                            <DropdownSlot appointments={props.appointments}/>

                        </Form.Control>
                    </Form.Group>




                    <Button className={'ml-2'} variant="primary" type="submit">
                        Add appointment
                    </Button>

                </Form>
            </Container>
        </>);

};

export default AppointmentCreateForm