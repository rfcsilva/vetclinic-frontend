import React from "react";
import {RichAppointment} from "../AppointmentsClient/reducers";
import {Button, Col, Container, Row} from "react-bootstrap";

const AppointmentVet = (props: { appointment: RichAppointment }) => {

    let myDate = new Date(props.appointment.appointment.start_time);

    let myStyle = {
        borderRadius: '15px'
    };

    return (
        <>
            <Container fluid={true} className={'p-3'}>
                <Row style={myStyle} className={'p-1'} >
                    <Col xs={2}>
                        <Row>
                            <h4>{props.appointment.pet.name}</h4>
                        </Row>
                        <Row>
                            <Col>
                                <Row>
                                    <h6>Species: {props.appointment.pet.species}</h6>
                                </Row>
                                <Row>
                                    <h6>Owner: {props.appointment.client.name}</h6>
                                </Row>
                                <Row>
                                    <h6>Date: {myDate.toLocaleDateString()} at {myDate.toLocaleTimeString()}</h6>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                    <Col xs={6} className={'mt-5'}>
                        <Button>
                            Start Appointment
                        </Button>
                    </Col>
                </Row>
            </Container>
        </>);

};

export default AppointmentVet