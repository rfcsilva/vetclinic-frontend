import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import TopNavbar from "../TopNavBar/TopNavbar";
import {connect} from "react-redux";
import {GlobalState} from "../App/reducers";

const  myStyle = {
    height: '5em'
};

const ProtoAppHeader = (props:{isSignedIn:boolean, userType:string }) => {

    return (
        <div className={"p-5"}>
            <Row>
                <Col><h1>NovaVet Clinic</h1></Col>
            </Row>
            <Row style={myStyle}>
                <Col className={'pt-2'}>
                    <TopNavbar isSignedIn={props.isSignedIn} userType={ props.userType}/>
                </Col>
            </Row>
        </div>
    );
};

const mapStateToProps = (state: GlobalState) => ({isSignedIn: state.signIn.isSignedIn, userType: state.signIn.user.type});
export const AppHeader = connect(mapStateToProps)(ProtoAppHeader);