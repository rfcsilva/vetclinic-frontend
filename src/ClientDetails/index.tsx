import React from "react";
import {Col, Row} from "react-bootstrap";
import {Client} from "../ClientListFiltered/reducers";
import {Link} from "react-router-dom";
import {GlobalState} from "../App/reducers";
import {Pet} from "../PetListFiltered/reducers";
import {findPet} from "../PetDetails";
import {connect} from "react-redux";


export function findClient(clients: Client[], id: number) {

    console.log('Searching for: ' + id);
    let toReturn = clients.find(p => p.id == id); //We know we should have used ===, but for some reason it did not work

    console.log(toReturn);

    if (toReturn === undefined)
        return {} as Client;
    else
        return toReturn as Client;
}


const ProtoClientDetails = (props: { clients: Client[], id: number}) => {

    console.log(props.clients);
    console.log(props.id);

    let client: Client = findClient(props.clients, props.id);

    console.log('yayayay');

    return (
        <>
            <Row>
                <Col className={'pl-5'}>
                    <Row>
                        <img src={client.picture}></img>
                    </Row>
                    <Row className={'mb-2'}>
                        <h4>{client.name}</h4>
                    </Row>
                    <Row>
                        <h6>Email: {client.email}</h6>
                    </Row>
                    <Row>
                        <h6>Cellphone: {client.cellPhone}</h6>
                    </Row>
                    <Row>
                        <h6>Address: {client.address}</h6>
                    </Row>
                    <Row>
                        <h6> <Link to={`/pets`}>Pets</Link></h6>
                    </Row>
                </Col>
            </Row>
        </>);

};

const mapStateToProps = (state: GlobalState, ownProps:any) => ({
    clients: state.clients.clients,
    id: ownProps.match.params.id
});

export const ClientDetails = connect(mapStateToProps)(ProtoClientDetails)