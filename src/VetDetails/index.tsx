import React from "react";
import {Button, Col, Row} from "react-bootstrap";
import {Vet} from "../VetListFiltered/reducers";

const VetDetails = (props: { vet: Vet }) => {

    return (
        <>
            <Row>
                <Col className={'pl-5'}>
                    <Row>
                        <img src={props.vet.picture}></img>
                    </Row>
                    <Row className={'mb-2'}>
                        <h4>{props.vet.name}</h4>
                    </Row>
                    <Row>
                        <h6>Email: {props.vet.email}</h6>
                    </Row>
                    <Row>
                        <h6>Employee number: {props.vet.employee_number}</h6>
                    </Row>
                    <Row>
                        <h6>Address: {props.vet.address}</h6>
                    </Row>
                    <Row>
                        <h6>Appointments</h6>
                    </Row>
                </Col>
            </Row>
        </>);

};

export default VetDetails