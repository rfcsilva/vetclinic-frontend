import {Action} from "redux";
import * as faker from "faker";
import {FILTER_VETS, FilterVetsAction, RECEIVE_VETS, ReceiveVetAction, REQUEST_VETS} from "./actions";

export interface Vet {picture: string,
    employee_number: number,
    id: number,
    name: string,
    username: string,
    password: string,
    email: string,
    cellphone: number,
    address: string }

export interface VetState { vets: Vet[], visibleVets:Vet[], isFetching: boolean }

export interface NewVet {picture: string,
    id: number,
    name: string,
    username: string,
    password: string,
    email: string,
    cellphone: number,
    address: string }

export function buildNewVet(name: string, username: string, password: string, email: string, cellphone: number, address: string): NewVet {
    return {picture:"", id: 0, name: name, username:username, password:password, email: email, cellphone: cellphone, address: address};
}

export function buildFakeVet( ): Vet {
    return { employee_number:0, picture:"", id: 0, name: faker.name.firstName(), username: "Cat", password: "PASS", email: 'Daqueles amarelos', cellphone: 298234432, address: "true"};
}

const initialState = {
    vets: [buildFakeVet()],
    visibleVets: [],
    isFetching:false,
};


function myFilter(filter:string, listVet:Vet[]): Vet[] {

    let returnApps = [];

    for(let i = 0; i< listVet.length; i++){
        if (listVet[i].name.toUpperCase().startsWith(filter.toUpperCase())){
            returnApps.push(listVet[i])
        }
    }

    return returnApps;

}

function filteredVetListReducer(state:VetState = initialState, action:Action):VetState {
    switch (action.type) {
        case FILTER_VETS:
            return {...state, isFetching:false, visibleVets: myFilter((action as FilterVetsAction).data, state.vets) };
        case REQUEST_VETS:
            return {...state, isFetching:true};
        case RECEIVE_VETS:



            return {...state, isFetching:false, vets: (action as ReceiveVetAction).data };
        default:
            return state
    }
}

export default filteredVetListReducer