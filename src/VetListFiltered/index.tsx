import {Col, Row} from "react-bootstrap";
import React, {ChangeEvent, useEffect, useState} from "react";
import {GlobalState} from "../App/reducers";
import {connect} from "react-redux";
import {BasicUser} from "../UserInfo/UserInfo";
import {Link} from "react-router-dom";
import {Vet} from "./reducers";
import {doFilter, fetchVets} from "./actions";

export const VetList = (props:{vets:Vet[]} ) =>
    <ul>
        { props.vets.map((vet:Vet) => <Link to={`/vets/${vet.id}`}> <li value={vet.id} key={vet.id}>{vet.name}</li> </Link>)}
    </ul>;

const ProtoFilteredVetList = (props:{vets: Vet[], visibleVets: Vet[], loadVets:(filter:string, user_id: number)=>void, user: BasicUser, getFiltered:(filter:string) => void}) => {

    const [ filter, setFilter] = useState("");
    //useEffect(() => props.loadVets(filter, props.user.id), [filter]);
    useEffect(() => props.getFiltered(filter), [filter]);


    let handle = (e:ChangeEvent<HTMLInputElement>) => setFilter(e.target.value);

    return (
        <Row>
            <Col className={'pl-5'}>
                <Row>
                    <label>Search by name: </label>
                    <input className={'ml-2'} onChange={handle} value={filter}/>
                </Row>
                <Row>
                    <VetList vets={props.visibleVets} />
                </Row>
            </Col>
        </Row>
    );

};

const mapStateToProps = (state:GlobalState) => ({vets:state.vets.vets, user:state.signIn.user, visibleVets:state.vets.visibleVets });
const mapDispatchToProps = (dispatch:any) => ({
    loadVets:(filter:string, user_id: number) => { dispatch(fetchVets(user_id, filter))},
    getFiltered:(filter:string) => {dispatch(doFilter(filter))}
});
export const FilteredVetList = connect(mapStateToProps,mapDispatchToProps)(ProtoFilteredVetList);