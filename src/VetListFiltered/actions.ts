import {Vet} from "./reducers";
import {Action} from "redux";
import {getData} from "../RestClient";

export const REQUEST_VETS = 'REQUEST_VETS';
export const RECEIVE_VETS = 'RECEIVE_VETS';
export const FILTER_VETS = 'FILTER_VETS';

export interface AddVetAction extends Action { name:string }
export interface ReceiveVetAction extends Action { data:Vet[] }
export interface FilterVetsAction extends Action { data:string }

export const requestVets = () => ({type: REQUEST_VETS});
export const receiveVets = (data:Vet[]) => ({type: RECEIVE_VETS, data:data});
export const filterVets = (data:string) => ({type: FILTER_VETS, data:data})


export function fetchVets(user_id: number, filter:string) {
    return (dispatch:any) => {
        dispatch(requestVets());
        return getData(`/api/vet`, [])
            .then(data => { data && dispatch(receiveVets(data.map( (v:{vet:Vet}) => v.vet ))) })
        // notice that there is an extra "pet" in the path above which is produced
        // in this particular implementation of the service. {pet: Pet, appointments:List<AppointmentDTO>}
    }
}

export function doFilter(filter:string) {
    return (dispatch: any) => {
        dispatch(filterVets(filter));
    }
}
