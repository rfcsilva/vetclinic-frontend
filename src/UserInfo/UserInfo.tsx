import React from "react";
import faker from 'faker'
import {Card, ListGroup, ListGroupItem} from "react-bootstrap";
import logo192 from '../Assets/logo192.png'


export interface User {
    id: number,
    name: string,
    username: string,
    email: string,
    cellPhone: string,
    address: string,
    picture: string,
    role: string
}

export interface BasicUser {
    id: number,
    username: string,
    name: string,
    type: string,
    picture: string
}

export interface GroupedEmployees {
    admins : User[],
    vets: User[]
}


export function buildBasicUser(username: string, name: string, type: string): BasicUser {
    return {id: 0, name: name, username: username, type: type, picture: ""};
}


function buildUsers(role: string, count: number) {
    let i = 0;
    let users: User[] = [];
    while (i <count ){
        let name : string = faker.name.firstName();
        users.push({name: name, email: name + "@gmail.com", cellPhone: faker.phone.phoneNumber('91#######'), id: i, username: name+faker.random.alphaNumeric(3), address: faker.address.streetName(), picture: "pic.jpg", role:role });
        i++;
    }

    return users;
}


export  const defaultEmployees: GroupedEmployees = { admins: buildUsers('admin', 2), vets: buildUsers('vet',4)  };


const UserInfo = (props: { user: User }) => {
    return (
        <Card style={{width: '18rem'}}>
            <Card.Img variant="top" src={logo192}/>
            <Card.Body>
                <Card.Title>{props.user.name}</Card.Title>
                <ListGroup className="list-group-flush">
                    <ListGroupItem>Email: {props.user.email}</ListGroupItem>
                    <ListGroupItem>Cellphone: {props.user.cellPhone}</ListGroupItem>
                </ListGroup>
            </Card.Body>
        </Card>);
};

export default UserInfo;
