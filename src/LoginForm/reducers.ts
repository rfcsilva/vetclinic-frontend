/**
 Copyright 2019 João Costa Seco, Eduardo Geraldo

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

import {Action} from "redux";
import {SIGN_IN, SIGN_OUT, SignInAction} from "./actions";
import {BasicUser} from "../UserInfo/UserInfo";
import jwtDecode from  "jwt-decode";
function checkIfTokenIsValid() {
    console.log("Loading User");
    return localStorage.getItem('jwt') != null;
}

function loadUser(): BasicUser {
    let user = localStorage.getItem('currentUser');
    if( user!= null)
        return JSON.parse(user) as BasicUser

    return {} as BasicUser;
}

export interface SignInState { isSignedIn: boolean, user: BasicUser }

const initialState = {isSignedIn: checkIfTokenIsValid(), user: loadUser() };

function signInReducer(state: SignInState = initialState, action:Action) {
    switch (action.type) {
        case SIGN_IN:
            let token = (action as SignInAction).data;
            if( token ) {

                // @ts-ignore
                let newUser: BasicUser = JSON.parse( jwtDecode(token.substring(7)).user );
                localStorage.setItem('jwt',token);
                localStorage.setItem('currentUser', JSON.stringify(newUser));

                return { isSignedIn: true, user: newUser as BasicUser};
            } else {
                return state;
            }
        case SIGN_OUT:
            localStorage.removeItem('jwt');
            return {...state, isSignedIn: false};
        default:
            return state;
    }
}

export default signInReducer