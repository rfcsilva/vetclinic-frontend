import {Action} from "redux";
import {deleteData, postData} from "../RestClient";
import {NewPet, Pet} from "../PetListFiltered/reducers";
import {addPet, removePet} from "../PetListFiltered/actions";

export const DELETE_PET = 'DELETE_PET';
export const DELETE_PET_REPLY_OK = 'DELETE_PET_REPLY_OK';
export const DELETE_PET_REPLY_ERROR = 'DELETE_PET_REPLY_ERROR';

export interface DeletePetAction extends Action { }
export interface PetDeletedReplyOk extends Action { data: Pet }
export interface PetDeletedReplyError extends Action { }

export const deletePet = () => ({type:DELETE_PET});
export const deletePetReplyOk = (data:Pet) => ({type: DELETE_PET_REPLY_OK, data:data});
export const deletePetReplyError = () => ({type: DELETE_PET_REPLY_ERROR});

export function requestPetDeletion(pet_id: number) {
    return (dispatch: any) => {
        dispatch(deletePet());
        return deleteData(`/api/pets/${pet_id}`)
            .then(data => {
                data && dispatch(deletePetReplyOk(data) && dispatch(removePet(data)))
            })
            .catch(() => dispatch(deletePetReplyError()))
    }
}