import React from "react";
import {Button, Col, Container, Row} from "react-bootstrap";
import {NewPet, Pet} from "../PetListFiltered/reducers";
import {GlobalState} from "../App/reducers";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {requestPetCreation} from "../PetCreateForm/actions";
import {requestPetDeletion} from "./actions";

export function findPet(pets: Pet[], id: number) {

    console.log('Searching for: ' + id);
    let toReturn = pets.find(p => p.id == id); //We know we should have used ===, but for some reason it did not work

    console.log(toReturn);

    if (toReturn === undefined)
        return {} as Pet;
    else
        return toReturn as Pet;
}

const ProtoPetDetails = (props: { pets: Pet[], id: number,  performPetDeletion: (pet_id:number) => void }) => {

    console.log(props.pets);
    console.log(props.id);

    let pet: Pet = findPet(props.pets, props.id);

    console.log('yoyoyoyoh');

    let submitHandler = () => {
        console.log("DELETING")
        props.performPetDeletion(pet.id);
    }

    return (
        <>
            <Container>
                <Row>
                    <Col>
                        <Row className={'mb-2'}>
                            <h4> {pet.name} </h4>
                        </Row>
                        <Row>
                            <h6>Age: {pet.age} months old</h6>
                        </Row>
                        <Row>
                            <h6>Species: {pet.species}</h6>
                        </Row>
                        <Row>
                            <h6>Description: {pet.description} </h6>
                        </Row>
                        <Row>
                            <h6><Link to={`/pets/${pet.id}/appointments`}>Appointments</Link></h6>
                        </Row>
                        <Row>
                            <h6>Owner:{pet.owner.name}</h6>
                        </Row>
                        <Row>
                            <h6>Medical notes: {pet.medical_notes} </h6>
                        </Row>
                    </Col>
                    <Col className={'ml-5'}>
                        <Row className={'mt-5'}>
                            <Link onClick={ () => {( localStorage.setItem('selectedPet', JSON.stringify(pet)))}} to={`/pets/${pet.id}/edit`}>
                                <Button className={'ml-2'} variant="primary" type="submit">
                                    Edit pet
                                </Button>
                            </Link>
                        </Row>
                        <Row className={'mt-4'}>
                            <Button onClick={ submitHandler } className={'ml-2'} variant="danger" type="submit">
                                Delete Pet
                            </Button>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </>);

};


//TypeScript #Claps Claps lol
// @ts-ignore
const mapStateToProps = (state: GlobalState, ownProps) => ({
    pets: state.pets.pets,
    id: ownProps.match.params.id
});
const mapDispatchToProps = (dispatch: any) => ({
    performPetDeletion: (pet_id:number) => {
        dispatch(requestPetDeletion(pet_id))
    }
});

export const PetDetails = connect(mapStateToProps, mapDispatchToProps)(ProtoPetDetails);
