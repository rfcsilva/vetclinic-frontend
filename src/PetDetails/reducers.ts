import {Action} from "redux";
import {DELETE_PET, DELETE_PET_REPLY_ERROR, DELETE_PET_REPLY_OK} from "./actions";

const initialState = {
    success: true,
    isPosting: false
};

function petDeleteReducer(state= initialState, action:Action) {
    switch (action.type) {
        case DELETE_PET:
            return {...state, isPosting:true};
        case DELETE_PET_REPLY_OK:
            return {...state, isFetching:false, success: true};
        case DELETE_PET_REPLY_ERROR:
            return {...state, isFetching:false, success: false};
        default:
            return state
    }
}

export default petDeleteReducer