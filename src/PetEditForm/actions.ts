import {Action} from "redux";
import {BasicPet, Pet} from "../PetListFiltered/reducers";
import {putData} from "../RestClient";

export const UPDATE_PET = 'CREATE_PET';
export const RECEIVE_PET_UPDATE_REPLY_OK = 'RECEIVE_PET_CREATION_REPLY';
export const RECEIVE_PET_UPDATE_REPLY_ERROR = 'RECEIVE_PET_CREATION_REPLY_ERROR';

export interface UpdatePetAction extends Action { }
export interface ReceivePetUpdateReplyOk extends Action { data: Pet }
export interface ReceivedPetUpdateReplyError extends Action { }

export const updatePet = () => ({type:UPDATE_PET});
export const receivePetUpdateReplyOk = (data:Pet) => ({type: RECEIVE_PET_UPDATE_REPLY_OK, data:data});
export const receivedPetReplyError = () => ({type: RECEIVE_PET_UPDATE_REPLY_ERROR});

export function requestPetUpdate(pet: BasicPet) {
    return (dispatch: any) => {
        console.log("Here");
        dispatch(updatePet());
        return putData(`/api/pets/${pet.id}`, pet)
            .then(data => {
                data && dispatch(receivePetUpdateReplyOk(data))
            })
            .catch(() => dispatch(receivedPetReplyError()))
    }
}