import {Action} from "redux";
import {RECEIVE_PET_UPDATE_REPLY_ERROR, RECEIVE_PET_UPDATE_REPLY_OK, UPDATE_PET} from "./actions";

export interface PetEditingState { edited: boolean, success: boolean, isPosting: boolean }

const initialState = {
    edited: false,
    success: false,
    isPosting: false
};

function petEditFormReducer(state: PetEditingState = initialState, action:Action) {
    switch (action.type) {
        case UPDATE_PET:
            console.log('YOlo');
            return {...state, isPosting:true, edited: true};
        case RECEIVE_PET_UPDATE_REPLY_OK:
            return {...state, isFetching:false, success: true};
        case RECEIVE_PET_UPDATE_REPLY_ERROR:
            return {...state, isFetching:false, success: false};
        default:
            return state
    }
}

export default petEditFormReducer