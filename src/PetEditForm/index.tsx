import {BasicPet, buildBasicPet, Pet} from "../PetListFiltered/reducers";
import {BasicUser} from "../UserInfo/UserInfo";
import React, {useState} from "react";
import {Button, Col, Container, Form, FormControl, FormControlProps, Row} from "react-bootstrap";
import {GlobalState} from "../App/reducers";
import {connect} from "react-redux";
import {findPet} from "../PetDetails";
import {requestPetUpdate} from "./actions";

const ProtoPetEditForm = (props: { edited: boolean, success: boolean, performPetUpdate: (pet: BasicPet) => void, currentUser: BasicUser, pets: Pet[], petId: number }) => {

    let pet: Pet = findPet(props.pets, props.petId);
    let ageString: string = pet.age.toString();

    const [name, setName] = useState(pet.name);
    const [species, setSpecies] = useState(pet.species);
    const [age, setAge] = useState(ageString);
    const [description, setDescription] = useState(pet.description);

    let nameChangeHandler = (e: React.FormEvent<FormControl & FormControlProps>) => {
        setName(e.currentTarget.value as string)
    };
    let speciesChangeHandler = (e: React.FormEvent<FormControl & FormControlProps>) => {
        setSpecies(e.currentTarget.value as string)
    };
    let ageChangeHandler = (e: React.FormEvent<FormControl & FormControlProps>) => {
        console.log('Vou por: ' + e.currentTarget.value as string);
        setAge(e.currentTarget.value as string)
        console.log(age);
    };
    let descriptionChangeHandler = (e: React.FormEvent<FormControl & FormControlProps>) => {
        setDescription(e.currentTarget.value as string)
    };

    let submitHandler2 = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        let newPet: BasicPet = buildBasicPet(pet.id, name, species, parseInt(age), description);
        props.performPetUpdate(newPet);

        setName("");
        setSpecies("");
        setAge("");
        setDescription("");
    };

    return (
        <>
            <Container>
                <Row>
                    <Col>

                        {props.edited && !props.success && <> <Row><h3>An Error!</h3></Row>  </>}
                        {props.edited && props.success && <> <Row><h3>An Error!</h3></Row>  </>}

                        <Form onSubmit={submitHandler2}>
                            <Form.Row>
                                <Form.Group as={Col} controlId="formGridName">
                                    <Form.Label>Pet name</Form.Label>
                                    <Form.Control onChange={nameChangeHandler} type="name"
                                                  placeholder={name}/>
                                </Form.Group>

                                <Form.Group as={Col} controlId="formGridSpecies">
                                    <Form.Label>Species</Form.Label>
                                    <Form.Control onChange={speciesChangeHandler} type="name" value={pet.species}
                                                  placeholder={species}/>
                                </Form.Group>
                            </Form.Row>

                            <Form.Group controlId="formGridAddress1">
                                <Form.Label>Age of your pet (in months)</Form.Label>
                                <Form.Control onChange={ageChangeHandler} type="number"
                                              min="1" placeholder={age}/>
                            </Form.Group>

                            <Form.Group controlId="formGridDescription">
                                <Form.Label>Brief physical description of your pet</Form.Label>
                                <Form.Control onChange={descriptionChangeHandler} as="textarea" rows="3"

                                              placeholder={description}/>
                            </Form.Group>
                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                        </Form>
                    </Col>
                </Row>
            </Container>
        </>);

};

//TypeScript #Claps Claps lol
// @ts-ignore
const mapStateToProps = (state: GlobalState, ownProps) => ({
    edited: state.petEdition.edited,
    success: state.petEdition.success,
    currentUser: state.signIn.user,
    pets: state.pets.pets,
    petId: ownProps.match.params.id
});
const mapDispatchToProps = (dispatch: any) => ({
    performPetUpdate: (pet: BasicPet) => {
        dispatch(requestPetUpdate(pet))
    }
});

const PetEditForm = connect(mapStateToProps, mapDispatchToProps)(ProtoPetEditForm);
export default PetEditForm