import React from "react";
import {RichAppointment} from "../AppointmentsClient/reducers";
import {Col, Row} from "react-bootstrap";
import {Pet} from "../PetListFiltered/reducers";
import {GlobalState} from "../App/reducers";
import {connect} from "react-redux";

export function findAppointment(apps: RichAppointment[], id: number) {

    console.log('Searching for: ' + id);
    console.log(apps);
    let toReturn = apps.find(p => p.appointment.id == id); //We know we should have used ===, but for some reason it did not work

    console.log(toReturn);

    if (toReturn === undefined)
        return {} as RichAppointment;
    else
        return toReturn as RichAppointment;
}


const ProtoAppointmentDetails = (props:{appointments: RichAppointment[], id: number}) => {

    let appointment = findAppointment(props.appointments, props.id)

    let myDate = new Date(appointment.appointment.start_time);
    let duraTion = new Date(appointment.appointment.duration)
    let myStyle={
        backgroundColor: 'blue'
    };

    return (
        <>
            <Row  className={'ml-3'}>
                <Col>
                    <Row className={'mb-2'}>
                        <h4>{myDate.toLocaleDateString()} at {myDate.toLocaleTimeString()}</h4>


                    </Row>
                    <Row>
                        <Col>
                            <Row>
                                <h6>Vet: {appointment.vet.name}</h6>
                            </Row>
                            <Row>
                                <h6>Pet: {appointment.pet.name}</h6>
                            </Row>
                            <Row>
                                <h6>Client: {appointment.client.name}</h6>
                            </Row>
                            <Row>
                                <h6>Duration: {duraTion.toLocaleTimeString()}</h6>
                            </Row>
                            <Row>
                                <h6>Description: {appointment.appointment.description}</h6>
                            </Row>
                            <Row>
                                <h6>Status: {appointment.appointment.status}</h6>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </>);

};

const mapStateToProps = (state: GlobalState, ownProps:any) => ({
    appointments: state.appointments.appointments,
    id: ownProps.match.params.id
});


export const AppointmentDetails = connect(mapStateToProps)(ProtoAppointmentDetails);