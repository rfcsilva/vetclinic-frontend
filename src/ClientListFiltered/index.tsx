import {Col, Row} from "react-bootstrap";
import React, {ChangeEvent, useEffect, useState} from "react";
import {GlobalState} from "../App/reducers";
import {connect} from "react-redux";
import {BasicUser} from "../UserInfo/UserInfo";
import {Link} from "react-router-dom";
import {fetchClients} from "./actions";
import {Client} from "./reducers";


export const ClientList = (props:{clients:Client[]} ) =>
    <ul>
        { props.clients.map((client:Client) =>  <li value={client.id} key={client.id}> <Link to={`/clients/${client.id}`}> {client.name} </Link> </li>)}
    </ul>;

const ProtoFilteredClientList = (props:{clients: Client[], loadClients:(filter:string, user_id: number)=>void, user: BasicUser}) => {

    console.log("tas ai dred?")
    const [ filter, setFilter] = useState("");
    useEffect(() => props.loadClients(filter, props.user.id), [filter]);
    console.log("rip?")

    let handle = (e:ChangeEvent<HTMLInputElement>) => setFilter(e.target.value);

    return (
        <Row>
            <Col className={'pl-5'}>
                <Row>
                    <label>Search by name: </label>
                    <input className={'ml-2'} onChange={handle} value={filter}/>
                </Row>
                <Row>
                    <ClientList clients={props.clients} />
                </Row>
            </Col>
        </Row>
    );

};

const mapStateToProps = (state:GlobalState) => ({clients:state.clients.clients, user:state.signIn.user});
const mapDispatchToProps = (dispatch:any) => ({
    loadClients:(filter:string, user_id: number) => { console.log("acorda caralho");dispatch(fetchClients(user_id, filter))}

});
export const FilteredClientList = connect(mapStateToProps,mapDispatchToProps)(ProtoFilteredClientList);