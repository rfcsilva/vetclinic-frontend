import {Action} from "redux";
import * as faker from "faker";
import {FILTER_CLIENTS, FilterClientsAction, RECEIVE_CLIENTS, ReceiveClientsAction, REQUEST_CLIENTS} from "./actions";
import {Pet} from "../PetListFiltered/reducers";
import {ReceivePetAction} from "../PetListFiltered/actions";


export interface Client {picture: string,
    id: number,
    name: string,
    username: string,
    email: string,
    cellPhone: number,
    address: string }

export interface ClientState { clients: Client[], isFetching: boolean }

export interface NewClient {picture: string,
    id: number,
    name: string,
    username: string,
    password: string,
    email: string,
    cellphone: number,
    address: string }

export function buildNewClient(client:Client): Client {
    return {picture:client.picture, id: client.id, name: client.name, username:client.username, email: client.email, cellPhone: client.cellPhone, address: client.address};
}

export function buildFakeClient( ): Client {
    return {picture:"", id: 0, name: faker.name.firstName(), username: "Cat", email: 'Daqueles amarelos', cellPhone: 298234432, address: "true"};
}

function myFilter(filter:string, listClient:Client[]): Client[] {

    let returnApps = [];

    for(let i = 0; i< listClient.length; i++){
        if (listClient[i].name.toUpperCase().startsWith(filter.toUpperCase())){
            returnApps.push(listClient[i])
        }
    }

    return returnApps;

}

function loadClients(): Client[] {
    let clientsJson = localStorage.getItem('clients');
    if(clientsJson === null)
        return [] as Client[];
    else
        return JSON.parse(clientsJson);
}

const initialState = {
    clients: loadClients(),
    isFetching:false
};

function filteredClientListReducer(state:ClientState = initialState, action:Action):ClientState {
    switch (action.type) {
        case REQUEST_CLIENTS:
            return {...state, isFetching:true};
        case RECEIVE_CLIENTS:
            let newClients : Client[] = (action as ReceiveClientsAction).data;
            newClients.map( p => console.log(p));

            localStorage.setItem('clients', JSON.stringify(newClients));

            return { isFetching:false, clients: (action as ReceiveClientsAction).data };
        default:
            return state
    }
}

export default filteredClientListReducer