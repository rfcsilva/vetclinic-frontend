import {buildNewClient, Client} from "./reducers";
import {Action} from "redux";
import {getData} from "../RestClient";
import {filterVets} from "../VetListFiltered/actions";

export const REQUEST_CLIENTS = 'REQUEST_CLIENTS';
export const RECEIVE_CLIENTS = 'RECEIVE_CLIENTS';
export const FILTER_CLIENTS = 'FILTER_CLIENTS';

export interface ReceiveClientsAction extends Action { data:Client[] }
export interface FilterClientsAction extends Action { data:string }

export const requestClient = () => ({type: REQUEST_CLIENTS});
export const receiveClient = (data:Client[]) => ({type: RECEIVE_CLIENTS, data:data});
export const filterClients = (data:string) => ({type: FILTER_CLIENTS, data:data});

export function fetchClients(user_id: number, filter:string) {
    console.log("Buscando quelientes")
    return (dispatch:any) => {
        dispatch(requestClient());
        return getData(`/api/clients`, [])
            .then(data => { data && dispatch(receiveClient(data.map(  c => buildNewClient(c) ))) })
        // notice that there is an extra "pet" in the path above which is produced
        // in this particular implementation of the service. {pet: Pet, appointments:List<AppointmentDTO>}
    }
}

export function doFilter(filter:string) {
    return (dispatch: any) => {
        dispatch(filterClients(filter));
    }
}
