import React from "react";
import {RichAppointment} from "../AppointmentsClient/reducers";
import {Col, Container, Row} from "react-bootstrap";

const Appointment = (props: { appointment: RichAppointment }) => {

    let myDate = new Date(props.appointment.appointment.start_time);

    let myStyle = {
        borderRadius: '15px'
    };

    return (
        <>
            <Container fluid={true} className={'p-3'}>
                <Row style={myStyle} className={'p-1'} >
                    <Col>
                        <Row>
                            <h4>{props.appointment.pet.name}</h4>
                        </Row>
                        <Row>
                            <Col>
                                <Row>
                                    <h6>Vet: {props.appointment.vet.name}</h6>
                                </Row>
                                <Row>
                                    <h6>Date: {myDate.toLocaleDateString()} at {myDate.toLocaleTimeString()}</h6>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </>);

};

export default Appointment